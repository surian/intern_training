import React, { useCallback, useEffect } from 'react';
import { LinearGradient } from 'expo-linear-gradient';
import {
  FlatList,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  Dimensions,
  Button,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import * as storeaction from '../../store/action/StoreActions'


const StoreScreen = props => {
    const stores = useSelector(state => state.StoreReducer.store)
    const dispatch = useDispatch();
    const loadstore = useCallback(async () => {
        try {
          await dispatch(storeaction.fetchStores());
        } catch (err) {
          console.log;
        }
      }, [dispatch]);
      useEffect(() => {
        loadstore();
      }, []);
      return (
          <View>
              <FlatList 
              data= {stores}
              keyExtractor={item => item.storeCode}
              renderItem={itemData => (
                  <View>
                      <Text>
                      {itemData.item.storeName}
                      </Text>
                  </View>
              )}
              />
          </View>
      )
      
 }


 export default StoreScreen;
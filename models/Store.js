class Store {
    constructor(storeCode, storeName) {
      this.storeCode = storeCode;
      this.storeName = storeName;
    }
  }

export default Store;
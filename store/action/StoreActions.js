import uri from '../../Api/Uri';
import Store from '../../models/Store';
export const VIEW_STORE = 'VIEW_STORE';

export const fetchStores = () => {

    return async dispatch => 
    {
        try{
            const response = await fetch(`http://${uri}/jasm/jasmapp/1.0.4/store/card/findByFranchise?franchiseid=1`,{
                method: 'GET',
            })
            if(!response.ok ){
                throw new Error('Somthing is wrong')
            }
            const resData = await response.json()
            const loadedStore = [];
            for (const key in resData) {
                loadedStore.push(new Store(resData[key].storecode, resData[key].name));
              }
            
            dispatch({ type: VIEW_STORE, stores: loadedStore });
            
        }catch (err){
            throw err
        }
        
    }
    
}